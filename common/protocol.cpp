// SPDX-FileCopyrightText: 2024 g10 code Gmbh
// SPDX-Contributor: Carl Schwan <carl.schwan@gnupg.com>
// SPDX-License-Identifier: GPL-2.0-or-later

#include "protocol.h"

#include <QHash>

using namespace Qt::StringLiterals;
using namespace Protocol;

Command Protocol::commandFromString(const QString &type)
{
    static const QHash<QString, Command> typeMap{
        {u"disconnection"_s, Command::Disconnection},
        {u"connection"_s, Command::Connection},
        {u"view"_s, Command::View},
        {u"register"_s, Command::Register},
        {u"log"_s, Command::Log},
        {u"reply"_s, Command::Reply},
        {u"forward"_s, Command::Forward},
        {u"composer"_s, Command::Composer},
        {u"open-draft"_s, Command::OpenDraft},
        {u"delete-draft"_s, Command::DeleteDraft},
        {u"restore-autosave"_s, Command::RestoreAutosave},
        {u"info"_s, Command::Info},
        {u"info-fetched"_s, Command::InfoFetched},
        {u"ews"_s, Command::Ews},
        {u"ews-response"_s, Command::EwsResponse},
        {u"viewer-closed"_s, Command::ViewerClosed},
        {u"viewer-opened"_s, Command::ViewerOpened},
    };

    return typeMap[type];
}

QString Protocol::commandToString(Command type)
{
    static const QHash<Command, QString> typeMap{
        {Command::Disconnection, u"disconnection"_s},
        {Command::Connection, u"connection"_s},
        {Command::View, u"view"_s},
        {Command::Register, u"register"_s},
        {Command::Log, u"log"_s},
        {Command::Reply, u"reply"_s},
        {Command::Forward, u"forward"_s},
        {Command::Composer, u"composer"_s},
        {Command::OpenDraft, u"open-draft"_s},
        {Command::DeleteDraft, u"delete-draft"_s},
        {Command::RestoreAutosave, u"restore-autosave"_s},
        {Command::Info, u"info"_s},
        {Command::InfoFetched, u"info-fetched"_s},
        {Command::EwsResponse, u"ews-response"_s},
        {Command::Ews, u"ews"_s},
        {Command::ViewerClosed, u"viewer-closed"_s},
        {Command::ViewerOpened, u"viewer-opened"_s},
    };

    return typeMap[type];
}
