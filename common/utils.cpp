// SPDX-FileCopyrightText: 2023 g10 code GmbH
// SPDX-Contributor: Carl Schwan <carl.schwan@gnupg.com>
// SPDX-License-Identifier: GPL-2.0-or-later

#include "utils.h"
#include <QList>

#if QT_VERSION >= QT_VERSION_CHECK(6, 8, 0)
QByteArray Utils::findHeader(const QHttpHeaders &headers, const QByteArray &key)
{
    return headers.value(key).toByteArray();
}
#else
QByteArray Utils::findHeader(QList<QPair<QByteArray, QByteArray>> headers, const QByteArray &key)
{
    const auto it = std::find_if(std::cbegin(headers), std::cend(headers), [&key](auto header) {
        return header.first == key;
    });

    if (it == std::cend(headers)) {
        return {};
    }

    return it->second;
}
#endif
