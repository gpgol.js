// SPDX-FileCopyrightText: 2023 g10 code GmbH
// SPDX-Contributor: Carl Schwan <carl.schwan@gnupg.com>
// SPDX-License-Identifier: GPL-2.0-or-later

#include <QByteArray>

#if QT_VERSION >= QT_VERSION_CHECK(6, 8, 0)
#include <QHttpHeaders>
#endif

namespace Utils
{
#if QT_VERSION >= QT_VERSION_CHECK(6, 8, 0)
QByteArray findHeader(const QHttpHeaders &headers, const QByteArray &key);
#else
QByteArray findHeader(QList<QPair<QByteArray, QByteArray>> headers, const QByteArray &key);
#endif
}
