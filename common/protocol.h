// SPDX-FileCopyrightText: 2024 g10 code Gmbh
// SPDX-Contributor: Carl Schwan <carl.schwan@gnupg.com>
// SPDX-License-Identifier: GPL-2.0-or-later

#pragma once

#include <QObject>
#include <QString>

namespace Protocol
{
Q_NAMESPACE

enum Command {
    Unknown, ///< Unknow action type
    Disconnection, ///< Disconnection of the web client
    Connection, ///< Connection or reconnection of the web client
    Register, ///< Registration of a client (native or web).
    Log, ///< Web client logs
    View, ///< View email in mailviewer
    Reply,
    Forward,
    Composer,
    OpenDraft,
    DeleteDraft,
    RestoreAutosave,
    Info,
    InfoFetched,
    Ews, ///< Send EWS request
    EwsResponse, ///< Receive EWS response
    ViewerClosed,
    ViewerOpened,
};
Q_ENUM_NS(Command);

Command commandFromString(const QString &type);
QString commandToString(Command type);

}
