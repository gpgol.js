# GPGol.js

This project provides a GnuPG integration for users of Outlook Web.

## Files you see here

- doc/architecture.odp :: Slides describing the architecture
- doc/security.md      :: Security assessment
- common/  :: Helper code
- server/  :: The broker part to connect Outlook with our client
- client/  :: The client representing our mail GUI


## Build it (Linux)

If you distribution provides a sufficient up to date version of the KDE
Frameworks 6 and QGpgME, the easiest is to install the development package from
your package manager. Otherwise, you can build all the dependencies yourself with
[kdesrc-build](https://community.kde.org/Get_Involved/development).

Then clone and build gpgol.js:

```sh
git clone https://dev.gnupg.org/source/gpgol.js.git
cd gpgol.js
cmake -GNinja -DCMAKE_EXPORT_COMPILE_COMMANDS=1 -DCMAKE_BUILD_TYPE=Debug -S . build
cmake --build build && cmake --install build
source build/prefix.sh
```

## Installing the Outlook extension

You can install the certificate by visiting the following address:
`https://outlook.office365.com/owa/?path=/options/manageapps`. This will load after
a short while the addon manager and you can upload the manifest in
`broker/manifest.xml` to the user defined Add-Ins.

## Install

Outlook requires the connections to be using HTTPS, so we need to generate a
self-signed TLS certificate. We use `mkcert` for this:

```sh
mkcert localhost 127.0.0.1
mkcert --install
```

### Linux

```sh
# copy to server
mkdir -p ~/.local/share/gpgol-server
cp localhost+1-key.pem ~/.local/share/gpgol-server/certificate-key.pem
cp localhost+1.pem ~/.local/share/gpgol-server/certificate.pem

# copy to client
mkdir -p ~/.local/share/gpgol-client
cp localhost+1.pem ~/.local/share/gpgol-client/certificate.pem
```

### Windows

```sh
# copy to server
mkdir C:\Users\<User>\AppData\Local\gpgol-server
cp localhost+1-key.pem C:\Users\<User>\AppData\Local\gpgol-server\certificate-key.pem
cp localhost+1.pem C:\Users\<User>\AppData\Local\gpgol-server\certificate.pem

# copy to client
mkdir C:\Users\<User>\AppData\Local\gpgol-client
cp localhost+1.pem C:\Users\<User>\AppData\Local\gpgol-client\certificate.pem
```

## Run it

The application consists of two process, you need to start both of them:

```sh
./build/bin/gpgol-server
```

and in a seperate terminal tab

```sh
./build/bin/gpgol-client
```
