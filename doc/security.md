# Man in the middle consideration

web -> broker -> native client -> broker -> web
                      ^
                      |
             Could be a fake pretending to be a real one and send emails on behalf of the user


web -> broker -> fake -> native client -> fake -> broker -> web
                   ^                        ^
                   |                        |
       Could be a fake and still forward all request

Assumption 0: While we are using a TLS connection with self-signed keys, all
the content is encrypted with the PGP keys of the user. Biggest issue would be
a fake native client trying to send fake mails on behalf of the user.

Assumption 1: a malicious webpages can do HTTP requests and open a websocket
connection to the broker/server, but can't handle incoming HTTP or websockets
requests. So the broker/server can not be faked by a webpages. For the native
client we need to ensure that the communication from the server to the native
client is only done via HTTP requests, the communication from the native client
to the server is authenticated and we only use the insecure websocket
connection to keep track on whether the service is running or not.

Assumption 2: the web outlook plugin is the only web process who can do HTTP
requests to the broker as we send a CORS header allowing only web pages served
from localhost:5656 to send request. This is not the case with websockets as
CORS headers are not supported for Websockets.

Assumption 3: A malicious service running on the computer itself which enough
power to listen to a port and handle HTTP request is not relevant here, since
at this state it is already game over. External service outside of the computer
can not contact the broker as the broker is listening on localhost.

## Sending an email

1. Web client send an HTTP request to open composer to server
2. Forward the request to the native client and add a HTTP header X-TOKEN which
   contains a random string. The token is stored by the server with a mapping
   from TOKEN to EMAIL
3. User write their message
4. When sending it, we encrypt and sign the email
5. Send HTTP request to server https://127.0.0.1:5656/socket-web which contains
   the X-TOKEN and X-EMAIL HTTP header.
6. Server check if the token has the corresponsing email. If not discard the
   request.
7. Forward the request via websocket to the web client
8. Web client creates the EWS requests
