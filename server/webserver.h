// SPDX-FileCopyrightText: 2023 g10 code GmbH
// SPDX-Contributor: Carl Schwan <carl.schwan@gnupg.com>
// SPDX-License-Identifier: GPL-2.0-or-later

#pragma once

#include <QByteArray>
#include <QList>
#include <QObject>
#include <QSslError>

QT_FORWARD_DECLARE_CLASS(QWebSocketServer)
QT_FORWARD_DECLARE_CLASS(QWebSocket)

class WebsocketRequestBackend;
class QHttpServer;
class QSslServer;

class WebServer : public QObject
{
    Q_OBJECT
public:
    explicit WebServer(QObject *parent = nullptr);
    ~WebServer();

    /// Start web server.
    bool run();

    /// is a valid WebServer instance
    bool isValid() const;

    bool sendMessageToWebClient(const QString &email, const QByteArray &payload);
    bool sendMessageToNativeClient(const QString &email, const QByteArray &payload);

private Q_SLOTS:
    void onNewConnection();
    void processTextMessage(QString message);
    void processBinaryMessage(QByteArray message);
    void socketDisconnected();

private:
    enum SpecialValues {
        Port = 5656,
        WebSocketPort = 5657,
    };

    void processCommand(const QJsonObject &object, QWebSocket *socket);

    QHttpServer *const m_httpServer;
    std::unique_ptr<QSslServer> m_tcpserver;
    QWebSocketServer *const m_webSocketServer;
    QList<QWebSocket *> m_clients;
    QHash<QString, QWebSocket *> m_webClientsMappingToEmail;
    QHash<QString, QWebSocket *> m_nativeClientsMappingToEmail;
};
