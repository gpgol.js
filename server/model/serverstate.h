// SPDX-FileCopyrightText: 2023 g10 code Gmbh
// SPDX-Contributor: Carl Schwan <carl.schwan@gnupg.com>
// SPDX-License-Identifier: GPL-2.0-or-later

#pragma once

#include <QHash>
#include <QNetworkAccessManager>
#include <QString>

using Token = QByteArray;
using Email = QString;

class ServerState
{
public:
    static ServerState &instance();

    QNetworkAccessManager qnam;

    QHash<Token, Email> composerRequest;

private:
    ServerState();
};
