// SPDX-FileCopyrightText: 2023 g10 code GmbH
// SPDX-Contributor: Carl Schwan <carl.schwan@gnupg.com>
// SPDX-License-Identifier: GPL-2.0-or-later

#pragma once

#include "abstractcontroller.h"

#include <QHttpServerRequest>

class DraftController : public AbstractController
{
public:
    static QHttpServerResponse listAction(const QHttpServerRequest &request);
    static QHttpServerResponse getAction(const QHttpServerRequest &request, QString);
};
