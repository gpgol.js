// SPDX-FileCopyrightText: 2023 g10 code GmbH
// SPDX-Contributor: Carl Schwan <carl.schwan@gnupg.com>
// SPDX-License-Identifier: GPL-2.0-or-later

#pragma once

#include <QHttpServerRequest>
#include <QHttpServerResponse>

class StaticController
{
public:
    static QHttpServerResponse homeAction(const QHttpServerRequest &request);
    static QHttpServerResponse assetsAction(QString fileName, const QHttpServerRequest &request);
};
