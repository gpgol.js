// SPDX-FileCopyrightText: 2024 g10 code Gmbh
// SPDX-Contributor: Carl Schwan <carl.schwan@gnupg.com>
// SPDX-License-Identifier: GPL-2.0-or-later

#include "firsttimedialog.h"
#include "config.h"
#include "gpgoljs_version.h"
#include "ui_firsttimedialog.h"
#include "websocketclient.h"

#include <QClipboard>
#include <QCloseEvent>
#include <QDesktopServices>

using namespace Qt::StringLiterals;

FirstTimeDialog::FirstTimeDialog(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::FirstTimeDialog)
{
    ui->setupUi(this);

    // center vertically
    ui->mainLayout->insertStretch(0);
    ui->mainLayout->addStretch();

    ui->mainLayoutManifest->insertStretch(0);
    ui->mainLayoutManifest->addStretch();

    const double titleFontSize = QApplication::font().pointSize() * 2;
    auto font = ui->title->font();
    font.setPointSize(titleFontSize);
    ui->title->setFont(font);

    QPixmap logo = QIcon::fromTheme(u"com.gnupg.gpgoljs"_s).pixmap(48, 48);
    ui->logo->setPixmap(logo);

    ui->version->setText(i18nc("@info", "Version: %1", QString::fromLocal8Bit(GPGOLJS_VERSION_STRING)));
    ui->state->setText(WebsocketClient::self().stateDisplay());
    connect(ui->setupButton, &QPushButton::clicked, this, [this]() {
        ui->stack->setCurrentIndex(1);
    });

    ui->manifestPath->setText(QLatin1StringView(DATAROUTDIR) + u"/gpgol/manifest.xml"_s);

    connect(ui->openOutlookButton, &QPushButton::clicked, this, []() {
        QDesktopServices::openUrl(QUrl(u"https://outlook.office.com/mail/jsmvvmdeeplink/?path=/options/manageapps&bO=4"_s));
    });

    connect(ui->manifestPathCopy, &QPushButton::clicked, this, [this]() {
        QGuiApplication::clipboard()->setText(ui->manifestPath->text());
    });
    ui->showOnStartup->setChecked(Config::self()->showLauncher());
    connect(ui->showOnStartup, &QCheckBox::toggled, this, [](bool checked) {
        Config::self()->setShowLauncher(checked);
        Config::self()->save();
    });
}

void FirstTimeDialog::slotStateChanged()
{
    ui->state->setText(WebsocketClient::self().stateDisplay());
}

void FirstTimeDialog::closeEvent(QCloseEvent *e)
{
    e->ignore();
    hide();
}

FirstTimeDialog::~FirstTimeDialog() = default;
