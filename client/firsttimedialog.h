// SPDX-FileCopyrightText: 2024 g10 code Gmbh
// SPDX-Contributor: Carl Schwan <carl.schwan@gnupg.com>
// SPDX-License-Identifier: GPL-2.0-or-later

#pragma once

#include <QMainWindow>

namespace Ui
{
class FirstTimeDialog;
}

class FirstTimeDialog : public QMainWindow
{
    Q_OBJECT
public:
    explicit FirstTimeDialog(QWidget *parent = nullptr);
    ~FirstTimeDialog();

public Q_SLOTS:
    void slotStateChanged();

protected:
    void closeEvent(QCloseEvent *e);

private:
    QScopedPointer<Ui::FirstTimeDialog> ui;
};
