// SPDX-FileCopyrightText: 2024 g10 code GmbH
// SPDX-FileContributor: Carl Schwan <carl.schwan@gnupg.com>
// SPDX-License-Identifier: LGPL-2.0-or-later

#include "messagedispatcher.h"

MessageDispatcher::MessageDispatcher(QObject *parent)
    : QObject(parent)
{
}
