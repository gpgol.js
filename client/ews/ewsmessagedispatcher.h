// SPDX-FileCopyrightText: 2024 g10 code GmbH
// SPDX-FileContributor: Carl Schwan <carl.schwan@gnupg.com>
// SPDX-License-Identifier: LGPL-2.0-or-later

#pragma once

#include "messagedispatcher.h"

/// Implementation of the MessageDispatcher for the ews service
///
/// This will wrap the MIME message inside a SOAP request and forward it
/// to the web addons.
class EWSMessageDispatcher : public MessageDispatcher
{
    Q_OBJECT

public:
    /// Default constructor
    explicit EWSMessageDispatcher(QObject *parent = nullptr);

    /// \copydoc MessageDispatcher::dispatch
    void dispatch(const KMime::Message::Ptr &message, const QString &from, const QString &mailId) override;

private:
    QByteArray m_bearerToken;
};
