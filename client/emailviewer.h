// SPDX-FileCopyrightText: 2024 g10 code GmbH
// SPDX-Contributor: Carl Schwan <carl.schwan@gnupg.com>
// SPDX-License-Identifier: GPL-2.0-or-later

#include <MimeTreeParserWidgets/MessageViewerWindow>

class EmailViewer : public MimeTreeParser::Widgets::MessageViewerWindow
{
    Q_OBJECT

public:
    explicit EmailViewer(const QString &content, const QString &accountEmail, const QString &displayName, const QString &bearerToken);

    void view(const QString &content, const QString &accountEmail, const QString &displayName, const QString &bearerToken);

protected:
    void closeEvent(QCloseEvent *event) override;
    void showEvent(QShowEvent *event) override;

private:
    QString m_email;
    QString m_displayName;
    QString m_bearerToken;
};
