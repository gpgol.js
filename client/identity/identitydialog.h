/*
    identitydialog.h

    This file is part of KMail, the KDE mail client.
    SPDX-FileCopyrightText: 2002 Marc Mutz <mutz@kde.org>

    SPDX-License-Identifier: GPL-2.0-only
*/

#pragma once

#include <QDialog>

class QCheckBox;

class KEditListWidget;
class QComboBox;
class QGroupBox;
class KJob;
class QLineEdit;
class QPushButton;
class QTabWidget;

namespace GpgME
{
class Key;
}
namespace KIdentityManagementCore
{
class Identity;
}
namespace KIdentityManagementWidgets
{
class SignatureConfigurator;
}
namespace Sonnet
{
class DictionaryComboBox;
}

namespace KMail
{
class IdentityFolderRequester;
class IdentityInvalidFolder;
class KeySelectionCombo;

class IdentityDialog : public QDialog
{
    Q_OBJECT
public:
    explicit IdentityDialog(QWidget *parent = nullptr);
    ~IdentityDialog() override;

    void setIdentity(/*_not_ const*/ KIdentityManagementCore::Identity &ident);

    void updateIdentity(KIdentityManagementCore::Identity &ident);

Q_SIGNALS:
    void keyListingFinished();

private:
    // copy default templates to identity templates
    void slotAccepted();
    void slotKeyListingFinished(KeySelectionCombo *combo);

    [[nodiscard]] bool keyMatchesEmailAddress(const GpgME::Key &key, const QString &email);
    [[nodiscard]] bool checkFolderExists(const QString &folder);

    QTabWidget *mTabWidget = nullptr;

    // "general" tab:
    QLineEdit *mNameEdit = nullptr;
    QLineEdit *mOrganizationEdit = nullptr;
    QLineEdit *mEmailEdit = nullptr;
    Sonnet::DictionaryComboBox *mDictionaryCombo = nullptr;

    // "cryptography" tab:
    QWidget *mCryptographyTab = nullptr;
    KeySelectionCombo *mPGPSigningKeyRequester = nullptr;
    KeySelectionCombo *mPGPEncryptionKeyRequester = nullptr;
    KeySelectionCombo *mSMIMESigningKeyRequester = nullptr;
    KeySelectionCombo *mSMIMEEncryptionKeyRequester = nullptr;
    QCheckBox *mPGPSameKey = nullptr;
    QCheckBox *mWarnNotEncrypt = nullptr;

    QSet<KeySelectionCombo *> mInitialLoadingFinished;

    // "signature" tab:
    KIdentityManagementWidgets::SignatureConfigurator *mSignatureConfigurator = nullptr;
};
} // namespace KMail
