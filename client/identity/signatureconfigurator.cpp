/*  -*- c++ -*-
    SPDX-FileCopyrightText: 2008 Thomas McGuire <Thomas.McGuire@gmx.net>
    SPDX-FileCopyrightText: 2008 Edwin Schepers <yez@familieschepers.nl>
    SPDX-FileCopyrightText: 2004 Marc Mutz <mutz@kde.org>

    SPDX-License-Identifier: LGPL-2.1-or-later
*/

#include "signatureconfigurator.h"
#include "editor/bodytexteditor.h"
#include "signaturerichtexteditor_p.h"

#include <KActionCollection>
#include <KLineEdit>
#include <KLocalizedString>
#include <KMessageBox>
#include <KToolBar>
#include <QUrl>

#include <QCheckBox>
#include <QComboBox>
#include <QDir>
#include <QFileInfo>
#include <QLabel>
#include <QStackedWidget>

#include <QHBoxLayout>
#include <QVBoxLayout>

#include <QStandardPaths>

using namespace KIdentityManagementWidgets;

namespace KIdentityManagementWidgets
{
/**
   Private class that helps to provide binary compatibility between releases.
   @internal
  */
//@cond PRIVATE
class Q_DECL_HIDDEN SignatureConfiguratorPrivate
{
public:
    explicit SignatureConfiguratorPrivate(SignatureConfigurator *parent);
    void init();
    // Returns the current text of the textedit as HTML code, but strips
    // unnecessary tags Qt inserts
    [[nodiscard]] QString asCleanedHTML() const;

    QString imageLocation;
    SignatureConfigurator *const q;
    QCheckBox *mEnableCheck = nullptr;
    MessageComposer::BodyTextEditor *mTextEdit = nullptr;
};
//@endcond

SignatureConfiguratorPrivate::SignatureConfiguratorPrivate(SignatureConfigurator *parent)
    : q(parent)
{
}

void SignatureConfiguratorPrivate::init()
{
    auto vlay = new QVBoxLayout(q);
    vlay->setObjectName(QLatin1StringView("main layout"));

    // "enable signature" checkbox:
    mEnableCheck = new QCheckBox(i18n("&Enable signature"), q);
    mEnableCheck->setWhatsThis(
        i18n("Check this box if you want KMail to append a signature to mails "
             "written with this identity."));
    vlay->addWidget(mEnableCheck);

    // "obtain signature text from" combo and label:
    auto hlay = new QHBoxLayout(); // inherits spacing
    vlay->addLayout(hlay);

    // widget stack that is controlled by the source combo:
    auto widgetStack = new QStackedWidget(q);
    widgetStack->setEnabled(false); // since !mEnableCheck->isChecked()
    vlay->addWidget(widgetStack, 1);
    // connects for the enabling of the widgets depending on
    // signatureEnabled:
    q->connect(mEnableCheck, &QCheckBox::toggled, widgetStack, &QStackedWidget::setEnabled);
    // The focus might be still in the widget that is disabled
    q->connect(mEnableCheck, &QCheckBox::clicked, mEnableCheck, qOverload<>(&QCheckBox::setFocus));

    int pageno = 0;
    // page 0: input field for direct entering:
    auto page = new QWidget(widgetStack);
    widgetStack->insertWidget(pageno, page);
    auto page_vlay = new QVBoxLayout(page);
    page_vlay->setContentsMargins(0, 0, 0, 0);

    mTextEdit = new MessageComposer::BodyTextEditor(q);

    page_vlay->addWidget(mTextEdit, 2);
    mTextEdit->setWhatsThis(i18n("Use this field to enter an arbitrary static signature."));

    hlay = new QHBoxLayout(); // inherits spacing
    page_vlay->addLayout(hlay);

    widgetStack->setCurrentIndex(0); // since mSourceCombo->currentItem() == 0
}

SignatureConfigurator::SignatureConfigurator(QWidget *parent)
    : QWidget(parent)
    , d(new SignatureConfiguratorPrivate(this))
{
    d->init();
}

SignatureConfigurator::~SignatureConfigurator() = default;

bool SignatureConfigurator::isSignatureEnabled() const
{
    return d->mEnableCheck->isChecked();
}

void SignatureConfigurator::setSignatureEnabled(bool enable)
{
    d->mEnableCheck->setChecked(enable);
}

Signature::Type SignatureConfigurator::signatureType() const
{
    return Signature::Inlined;
}

void SignatureConfigurator::setInlineText(const QString &text)
{
    d->mTextEdit->setText(text);
}

Signature SignatureConfigurator::signature() const
{
    Signature sig;
    const Signature::Type sigType = signatureType();
    switch (sigType) {
    case Signature::Inlined:
        sig.setText(d->mTextEdit->toPlainText());
        break;
    case Signature::Disabled:
        /* do nothing */
        break;
    }
    sig.setEnabledSignature(isSignatureEnabled());
    sig.setType(sigType);
    return sig;
}

void SignatureConfigurator::setSignature(const Signature &sig)
{
    setSignatureEnabled(sig.isEnabledSignature());

    // Let insertIntoTextEdit() handle setting the text, as that function also adds the images.
    d->mTextEdit->clear();
    SignatureRichTextEditor::insertIntoTextEdit(sig, Signature::Start, Signature::AddNothing, d->mTextEdit, true);
}
}

#include "moc_signatureconfigurator.cpp"
