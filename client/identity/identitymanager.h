// SPDX-FileCopyrightText: 2023 g10 code Gmbh
// SPDX-Contributor: Carl Schwan <carl.schwan@gnupg.com>
// SPDX-License-Identifier: GPL-2.0-or-later

#pragma once

#include "identity.h"

class KConfig;

class IdentityManager : public QObject
{
    Q_OBJECT

public:
    static IdentityManager &self();

    KIdentityManagementCore::Identity fromEmail(const QString &email, bool &isNew);
    KIdentityManagementCore::Identity fromUoid(uint uid);
    void updateIdentity(KIdentityManagementCore::Identity identity);

    KIdentityManagementCore::Identity::List identities() const;

    QStringList identityEmails() const;

    void writeConfig() const;
    void readConfig(KConfig *config);

Q_SIGNALS:
    void identitiesWereChanged();

private:
    QStringList groupList(KConfig *config) const;

    IdentityManager();
    ~IdentityManager();

    KIdentityManagementCore::Identity::List mIdentities;
    KConfig *mConfig = nullptr;
};