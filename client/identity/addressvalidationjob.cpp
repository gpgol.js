/*
 * This file is part of KMail.
 *
 * SPDX-FileCopyrightText: 2010 KDAB
 *
 * SPDX-FileContributor: Tobias Koenig <tokoe@kde.org>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include "addressvalidationjob.h"

#include <KLocalizedString>
#include <KMessageBox>

#include <KEmailAddress>

AddressValidationJob::AddressValidationJob(const QString &emailAddresses, QWidget *parentWidget, QObject *parent)
    : KJob(parent)
    , mEmailAddresses(emailAddresses)
    , mParentWidget(parentWidget)
{
}

AddressValidationJob::~AddressValidationJob() = default;

void AddressValidationJob::setDefaultDomain(const QString &domainName)
{
    mDomainDefaultName = domainName;
}

void AddressValidationJob::start()
{
    QString brokenAddress;
    const KEmailAddress::EmailParseResult errorCode = KEmailAddress::isValidAddressList(mEmailAddresses, brokenAddress);
    if (!(errorCode == KEmailAddress::AddressOk || errorCode == KEmailAddress::AddressEmpty)) {
        const QString errorMsg(QLatin1String("<qt><p><b>") + brokenAddress + QLatin1String("</b></p><p>") + KEmailAddress::emailParseResultToString(errorCode)
                               + QLatin1String("</p></qt>"));
        KMessageBox::error(mParentWidget, errorMsg, i18nc("@title:window", "Invalid Email Address"));
        mIsValid = false;
    } else {
        mIsValid = true;
    }

    emitResult();
}

bool AddressValidationJob::isValid() const
{
    return mIsValid;
}

#include "moc_addressvalidationjob.cpp"
