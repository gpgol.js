/*  -*- c++ -*-
    SPDX-FileCopyrightText: 2008 Thomas McGuire <Thomas.McGuire@gmx.net>
    SPDX-FileCopyrightText: 2008 Edwin Schepers <yez@familieschepers.nl>
    SPDX-FileCopyrightText: 2008 Tom Albers <tomalbers@kde.nl>
    SPDX-FileCopyrightText: 2004 Marc Mutz <mutz@kde.org>

    SPDX-License-Identifier: LGPL-2.1-or-later
*/

#pragma once

#include "identity/signature.h" // for Signature::Type
#include <QWidget>
#include <memory>

using KIdentityManagementCore::Signature;

namespace KIdentityManagementWidgets
{
class SignatureConfiguratorPrivate;
/**
 * This widget gives an interface so users can edit their signature.
 * You can set a signature via setSignature(), let the user edit the
 * signature and when done, read the signature back.
 */
class SignatureConfigurator : public QWidget
{
    Q_OBJECT
public:
    /**
     * Constructor
     */
    explicit SignatureConfigurator(QWidget *parent = nullptr);

    /**
     * destructor
     */
    ~SignatureConfigurator() override;

    /**
     * Indicated if the user wants a signature
     */
    [[nodiscard]] bool isSignatureEnabled() const;

    /**
     * Use this to activate the signature.
     */
    void setSignatureEnabled(bool enable);

    /**
     * This returns the type of the signature,
     * so that can be Disabled, Inline, fromFile, etc.
     */
    [[nodiscard]] Signature::Type signatureType() const;

    /**
     * Make @p text the text for the signature.
     */
    void setInlineText(const QString &text);

    /**
     * Returns the file url which the user wants
     * to use as a signature.
     */
    [[nodiscard]] QString filePath() const;

    /**
     * Set @p url for the file url part of the
     * widget.
     */
    void setFileURL(const QString &url);

    /**
     * Returns the url of the command which the
     * users wants to use as signature.
     */
    [[nodiscard]] QString commandPath() const;

    /**
     * Sets @p url as the command to execute.
     */
    void setCommandURL(const QString &url);

    /**
       Convenience method.
       @return a Signature object representing the state of the widgets.
     **/
    [[nodiscard]] Signature signature() const;

    /**
       Convenience method. Sets the widgets according to @p sig
       @param sig the signature to configure
    **/
    void setSignature(const Signature &sig);

private:
    void slotUrlChanged();
    void slotEdit();

    //@cond PRIVATE
    friend class SignatureConfiguratorPrivate;
    std::unique_ptr<SignatureConfiguratorPrivate> const d;
    //@endcond
};
}
