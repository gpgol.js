// SPDX-FileCopyrightText: 2007, 2009 Klarälvdalens Datakonsult AB
// SPDX-License-Identifier: GPL-2.0-or-later

#pragma once

#include <QSystemTrayIcon>

#include <memory>

class SystemTrayIcon : public QSystemTrayIcon
{
    Q_OBJECT
public:
    explicit SystemTrayIcon(QObject *parent = nullptr);
    explicit SystemTrayIcon(const QIcon &icon, QObject *parent = nullptr);
    ~SystemTrayIcon() override;

    void setMainWindow(QWidget *w);
    QWidget *mainWindow() const;

    void setAttentionWindow(QWidget *w);
    QWidget *attentionWindow() const;

    QIcon attentionIcon() const;
    QIcon normalIcon() const;
    bool attentionWanted() const;

public Q_SLOTS:
    void setAttentionIcon(const QIcon &icon);
    void setNormalIcon(const QIcon &icon);
    void setAttentionWanted(bool);
    void slotAbout();
    void slotStateChanged();

protected Q_SLOTS:
    virtual void slotEnableDisableActions();

private:
    virtual void doMainWindowSet(QWidget *);
    virtual void doMainWindowClosed(QWidget *);
    virtual void doAttentionWindowClosed(QWidget *);
    virtual void doActivated();

private:
    bool eventFilter(QObject *, QEvent *) override;

private:
    class Private;
    const std::unique_ptr<Private> d;
};
