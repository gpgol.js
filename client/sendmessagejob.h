// SPDX-FileCopyrightText: 2024 g10 code GmbH
// SPDX-FileContributor: Carl Schwan <carl.schwan@gnupg.com>
// SPDX-License-Identifier: LGPL-2.0-or-later

#pragma once

#include <KJob>
#include <KMime/Message>

class SendMessageJob : public KJob
{
    Q_OBJECT

public:
    /// Get the message that will be sent
    KMime::Message::Ptr message() const;

    /// Set the message that will be sent
    void setMessage(const KMime::Message::Ptr &message);

private:
    KMime::Message::Ptr m_message;
};
