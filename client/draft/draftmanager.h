// SPDX-FileCopyrightText: 2023 g10 code GmbH
// SPDX-Contributor: Carl Schwan <carl.schwan@gnupg.com>
// SPDX-License-Identifier: GPL-2.0-or-later

#pragma once

#include "draft.h"
#include <QJsonArray>

/// @class DraftManager
///
/// Manage the email drafts of the user.
class DraftManager
{
public:
    /// Get the DraftManager singleton.
    /// \param testMode Set it to true when running unit tests.
    static DraftManager &self(bool testMode = false);

    /// Get the directory where the drafts are stored.
    /// \param testMode Set it to true when running unit tests.
    static QString draftDirectory(bool testMode = false);

    /// Get the directory where the autosaved drafts are stored.
    /// \param testMode Set it to true when running unit tests.
    static QString autosaveDirectory(bool testMode = false);

    /// List of drafts.
    [[nodiscard]] QList<Draft> drafts() const;

    /// List of autosave.
    [[nodiscard]] QList<Draft> autosaves() const;

    /// List of drafts as JSON array.
    [[nodiscard]] QJsonArray toJson() const;

    /// Remove the specified dradt from the filesystem.
    bool remove(const Draft &draft);

    /// Get a draft by it's id.
    Draft draftById(const QByteArray &draftId);

private:
    DraftManager(bool testMode);
    QList<Draft> m_drafts;
    bool m_testMode = false;
};