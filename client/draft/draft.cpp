// SPDX-FileCopyrightText: 2023 g10 code GmbH
// SPDX-Contributor: Carl Schwan <carl.schwan@gnupg.com>
// SPDX-License-Identifier: GPL-2.0-or-later

#include "draft.h"

#include <QFile>
#include <MimeTreeParserCore/FileOpener>

#include "editor_debug.h"

using namespace Qt::Literals::StringLiterals;

Draft::Draft(const QString &localUrl)
    : m_localUrl(localUrl)
    , m_fileInfo(m_localUrl)
{
}

bool Draft::isValid() const
{
    return m_fileInfo.exists() && m_fileInfo.isReadable();
}

QJsonObject Draft::toJson() const
{
    return {
        {"id"_L1, m_fileInfo.fileName()},
        {"url"_L1, m_localUrl},
        {"last_modification"_L1, lastModified().toSecsSinceEpoch()},
    };
}

QString Draft::id() const
{
    return m_fileInfo.fileName();
}

QString Draft::localUrl() const
{
    return m_localUrl;
}

QDateTime Draft::lastModified() const
{
    return m_fileInfo.lastModified();
}

bool Draft::remove()
{
    if (!m_fileInfo.exists()) {
        qCWarning(EDITOR_LOG) << "File doesn't exist anymore.";
        return false;
    }
    QFile file(m_fileInfo.filePath());
    return file.remove();
}

KMime::Message::Ptr Draft::mime() const
{
    Q_ASSERT(isValid()); // should be checked by the caller

    auto mimes = MimeTreeParser::Core::FileOpener::openFile(m_fileInfo.filePath());
    if (mimes.size() != 1) {
        qFatal() << "Can open file" << m_fileInfo.filePath();
    }
    return mimes[0];
}
