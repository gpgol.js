// SPDX-FileCopyrightText: 2023 g10 code GmbH
// SPDX-Contributor: Carl Schwan <carl.schwan@gnupg.com>
// SPDX-License-Identifier: GPL-2.0-or-later

#pragma once

#include <QFileInfo>
#include <QJsonObject>
#include <QString>

#include <KMime/Message>

class DraftManager;

/// @class Draft
/// Represent an email draft or autosave in the filesytem.
class Draft
{
public:
    /// Load the draft from it's local uri.
    explicit Draft(const QString &localUrl);

    /// Returns whether this Draft object is valid.
    bool isValid() const;

    /// Convert the draft meta information to a JSON object.
    QJsonObject toJson() const;

    /// Local url of the draft in the filesystem.
    QString localUrl() const;

    /// The mail id of the draft.
    QString id() const;

    QDateTime lastModified() const;

    KMime::Message::Ptr mime() const;

private:
    friend DraftManager;

    /// Remove the draft from the filesystem.
    bool remove();

    QString m_localUrl;
    QFileInfo m_fileInfo;
};

inline bool operator==(const Draft &lhs, const Draft &rhs)
{
    return lhs.localUrl() == rhs.localUrl();
}
