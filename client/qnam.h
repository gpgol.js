// SPDX-FileCopyrightText: 2023 g10 code Gmbh
// SPDX-Contributor: Carl Schwan <carl.schwan@gnupg.com>
// SPDX-License-Identifier: GPL-2.0-or-later

#include <QNetworkAccessManager>

Q_GLOBAL_STATIC(QNetworkAccessManager, qnam);
