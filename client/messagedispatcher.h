// SPDX-FileCopyrightText: 2024 g10 code GmbH
// SPDX-FileContributor: Carl Schwan <carl.schwan@gnupg.com>
// SPDX-License-Identifier: LGPL-2.0-or-later

#pragma once

#include <KMime/Message>
#include <QObject>

class MessageDispatcher : public QObject
{
    Q_OBJECT

public:
    /// Default constructor
    explicit MessageDispatcher(QObject *parent = nullptr);

    /// Get the message that will be sent
    ///
    /// Emits sentSucessfully when done
    /// \param message The message to send
    /// \param from The sender email address
    /// \param from The mail id of the message
    virtual void dispatch(const KMime::Message::Ptr &message, const QString &from, const QString &mailId) = 0;

Q_SIGNALS:
    /// Message sending completed successfully.
    void sentSuccessfully(const QString &mailId);

    /// Message sending completed successfully.
    void errorOccurred(const QString &mailId, const QString &errorMessage);
};
