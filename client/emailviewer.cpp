// SPDX-FileCopyrightText: 2024 g10 code GmbH
// SPDX-Contributor: Carl Schwan <carl.schwan@gnupg.com>
// SPDX-License-Identifier: GPL-2.0-or-later

#include "emailviewer.h"

#include "editor/composerwindow.h"
#include "editor/composerwindowfactory.h"
#include "websocketclient.h"

#include <KLocalizedString>

#include <QMenuBar>
#include <QToolBar>
#include <QToolButton>

using namespace Qt::Literals::StringLiterals;

static KMime::Message::Ptr toMessage(const QString &content)
{
    KMime::Message::Ptr message(new KMime::Message());
    message->setContent(KMime::CRLFtoLF(content.toUtf8()));
    message->parse();
    return message;
}

EmailViewer::EmailViewer(const QString &content, const QString &accountEmail, const QString &displayName, const QString &bearerToken)
    : MimeTreeParser::Widgets::MessageViewerWindow()
    , m_email(accountEmail)
    , m_displayName(displayName)
    , m_bearerToken(bearerToken)
{
    setMessages({toMessage(content)});

    const auto message = messages().at(0);
    toolBar()->show();

    // spacer
    QWidget *spacer = new QWidget();
    spacer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    toolBar()->addWidget(spacer);

    auto messageMenu = menuBar()->findChild<QMenu *>("messageMenu");
    Q_ASSERT(messageMenu);

    messageMenu->addSeparator();

    // reply
    auto replyAction = new QAction(QIcon::fromTheme(u"mail-reply-sender-symbolic"_s), i18nc("@action:button", "Reply"), toolBar());
    connect(replyAction, &QAction::triggered, this, [message, this](bool) {
        auto dialog = ComposerWindowFactory::self().create(m_email, m_displayName, m_bearerToken.toUtf8());
        dialog->reply(message);
        dialog->show();
        dialog->activateWindow();
        dialog->raise();
    });
    toolBar()->addAction(replyAction);
    messageMenu->addAction(replyAction);
    auto widget = qobject_cast<QToolButton *>(toolBar()->widgetForAction(replyAction));
    widget->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);

    // forward
    auto forwardAction = new QAction(QIcon::fromTheme(u"mail-forward-symbolic"_s), i18nc("@action:button", "Forward"), toolBar());
    connect(forwardAction, &QAction::triggered, this, [message, this](bool) {
        auto dialog = ComposerWindowFactory::self().create(m_email, m_displayName, m_bearerToken.toUtf8());
        dialog->reply(message);
        dialog->show();
        dialog->activateWindow();
        dialog->raise();
    });
    toolBar()->addAction(forwardAction);
    messageMenu->addAction(forwardAction);
    widget = qobject_cast<QToolButton *>(toolBar()->widgetForAction(forwardAction));
    widget->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
}

void EmailViewer::closeEvent(QCloseEvent *event)
{
    MimeTreeParser::Widgets::MessageViewerWindow::closeEvent(event);

    WebsocketClient::self().sendViewerClosed(m_email);
}

void EmailViewer::showEvent(QShowEvent *event)
{
    MimeTreeParser::Widgets::MessageViewerWindow::showEvent(event);

    WebsocketClient::self().sendViewerOpened(m_email);
}

void EmailViewer::view(const QString &content, const QString &accountEmail, const QString &displayName, const QString &bearerToken)
{
    m_email = accountEmail;
    m_displayName = displayName;
    m_bearerToken = bearerToken;

    setMessages({toMessage(content)});
}
