/*
  This file is part of libkdepim.

  SPDX-FileCopyrightText: 2002 Helge Deller <deller@gmx.de>
  SPDX-FileCopyrightText: 2002 Lubos Lunak <llunak@suse.cz>
  SPDX-FileCopyrightText: 2001, 2003 Carsten Pfeiffer <pfeiffer@kde.org>
  SPDX-FileCopyrightText: 2001 Waldo Bastian <bastian@kde.org>
  SPDX-FileCopyrightText: 2004 Daniel Molkentin <danimo@klaralvdalens-datakonsult.se>
  SPDX-FileCopyrightText: 2004 Karl-Heinz Zimmer <khz@klaralvdalens-datakonsult.se>
  SPDX-FileCopyrightText: 2017-2024 Laurent Montel <montel@kde.org>

  SPDX-License-Identifier: LGPL-2.0-or-later
*/

#pragma once

#include <KLineEdit>

namespace KContacts
{
class Addressee;
class ContactGroup;
}

class AddresseeLineEditPrivate;

class AddresseeLineEdit : public KLineEdit
{
    Q_OBJECT

public:
    explicit AddresseeLineEdit(QWidget *parent = nullptr);
    ~AddresseeLineEdit();

    /**
     * Adds a new @p contact to the completion with a given
     * @p weight
     * @p source index
     * @p append  is added to completion string, but removed, when mail is selected.
     */
    void addContact(const KContacts::Addressee &contact, int weight, int source = -1, const QString &append = QString());

    /**
     * Same as the above, but this time with contact groups.
     */
    void addContactGroup(const KContacts::ContactGroup &group, int weight, int source = -1);

    [[nodiscard]] bool isCompletionEnabled() const;

    void callUserCancelled(const QString &str);
    void callSetCompletedText(const QString & /*text*/, bool /*marked*/);
    void callSetCompletedText(const QString &text);
    void callSetUserSelection(bool);

public Q_SLOTS:
    /**
     * Moves the cursor at the end of the line edit.
     */
    void cursorAtEnd();

    /**
     * Sets whether autocompletion shall be enabled.
     */
    void enableCompletion(bool enable);

    /**
     * Reimplemented for stripping whitespace after completion
     * Danger: This is _not_ virtual in the base class!
     */
    void setText(const QString &text) override;

    void emitTextCompleted();

    void slotEditingFinished();

Q_SIGNALS:
    void textCompleted();

protected:
    /**
     * Reimplemented for smart insertion of email addresses.
     * Features:
     * - Automatically adds ',' if necessary to separate email addresses
     * - Correctly decodes mailto URLs
     * - Recognizes email addresses which are protected against address
     *   harvesters, i.e. "name at kde dot org" and "name(at)kde.org"
     */
    virtual void insert(const QString &);

    /**
     * Reimplemented for smart insertion of pasted email addresses.
     */
    virtual void paste();

    /**
     * Reimplemented for smart insertion with middle mouse button.
     */
    void mouseReleaseEvent(QMouseEvent *) override;

    /**
     * Reimplemented for internal reasons.
     */
    void keyPressEvent(QKeyEvent *) override;

    bool eventFilter(QObject *object, QEvent *event) override;

    void insertEmails(const QStringList &emails);
    void dropEvent(QDropEvent *event) override;

private:
    void addContact(const QStringList &emails, const KContacts::Addressee &addr, int weight, int source, QString append = QString());

    std::unique_ptr<AddresseeLineEditPrivate> d;
};
