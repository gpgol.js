/*
  SPDX-FileCopyrightText: 2015-2023 Laurent Montel <montel@kde.org>

  SPDX-License-Identifier: GPL-2.0-or-later
*/

#pragma once

#include "../attachment/attachmentloadjob.h"

namespace MessageComposer
{
/**
 * @brief The AttachmentClipBoardJob class
 * @author Laurent Montel <montel@kde.org>
 */
class AttachmentClipBoardJob : public MessageCore::AttachmentLoadJob
{
    Q_OBJECT
public:
    explicit AttachmentClipBoardJob(QObject *parent = nullptr);
    ~AttachmentClipBoardJob() override;

protected Q_SLOTS:
    void doStart() override;

private:
    void addAttachment(const QByteArray &data, const QString &attachmentName);
};
}
