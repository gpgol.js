// SPDX-FileCopyrightText: 2009 Constantin Berzan <exit3219@gmail.com>
// SPDX-FileCopyrightText: 2023 g10 code GmbH
// SPDX-Contributor: Carl Schwan <carl.schwan@gnupg.com>
// SPDX-License-Identifier: LGPL-2.0-or-later

#pragma once

#include <Libkleo/Enum>

#include <QList>
#include <QStringList>

#include <KMime/Message>

#include <gpgme++/key.h>
#include <vector>

#include "editor/attachment/attachmentpart.h"
#include "jobbase.h"

namespace MessageComposer
{

class ComposerJobPrivate;
class GlobalPart;
class InfoPart;
class TextPart;
class ItipPart;

class ComposerJob : public JobBase
{
    Q_OBJECT

public:
    explicit ComposerJob(QObject *parent = nullptr);
    ~ComposerJob() override;

    [[nodiscard]] QList<KMime::Message::Ptr> resultMessages() const;

    [[nodiscard]] GlobalPart *globalPart() const;
    [[nodiscard]] InfoPart *infoPart() const;
    [[nodiscard]] TextPart *textPart() const;
    [[nodiscard]] ItipPart *itipPart() const;
    void clearTextPart();
    void clearItipPart();
    [[nodiscard]] MessageCore::AttachmentPart::List attachmentParts() const;
    void addAttachmentPart(MessageCore::AttachmentPart::Ptr part, bool autoresizeImage = false);
    void addAttachmentParts(const MessageCore::AttachmentPart::List &parts, bool autoresizeImage = false);
    void removeAttachmentPart(MessageCore::AttachmentPart::Ptr part);

    // if the message and attachments should not be encrypted regardless of settings
    void setNoCrypto(bool noCrypto);
    void setSignAndEncrypt(const bool doSign, const bool doEncrypt);
    void setCryptoMessageFormat(Kleo::CryptoMessageFormat format);
    void setSigningKeys(const std::vector<GpgME::Key> &signers);
    void setEncryptionKeys(const QList<QPair<QStringList, std::vector<GpgME::Key>>> &data);

    void setSenderEncryptionKey(const GpgME::Key &senderKey);

    void setGnupgHome(const QString &path);
    [[nodiscard]] QString gnupgHome() const;

    /// Sets if this message being composed is an auto-saved message.
    /// If so, it will be handled differently like without support for crypto attachment.
    void setAutoSave(bool isAutoSave);
    [[nodiscard]] bool autoSave() const;

    /// Sets if this message being composed is a draft message.
    /// If so, it will be handled differently like with only encrypted for the sender.
    void setDraft(bool isAutoSave);
    [[nodiscard]] bool draft() const;

    [[nodiscard]] bool finished() const;

public Q_SLOTS:
    void start() override;

protected Q_SLOTS:
    void slotResult(KJob *job) override;
    void doStart();
    void contentJobFinished(KJob *);
    void attachmentsFinished(KJob *);

private:
    Q_DECLARE_PRIVATE(ComposerJob)
};

}
