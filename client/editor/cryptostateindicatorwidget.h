/*
   SPDX-FileCopyrightText: 2014-2023 Laurent Montel <montel@kde.org>

   SPDX-License-Identifier: GPL-2.0-or-later
*/

#pragma once

#include <QWidget>

class QLabel;

class CryptoStateIndicatorWidget : public QWidget
{
    Q_OBJECT
public:
    explicit CryptoStateIndicatorWidget(QWidget *parent = nullptr);
    ~CryptoStateIndicatorWidget() override;

    void updateSignatureAndEncrypionStateIndicators(bool isSign, bool isEncrypted);

private:
    QLabel *const mSignatureStateIndicator;
    QLabel *const mEncryptionStateIndicator;
    bool mIsSign = false;
    bool mIsEncrypted = false;
};
