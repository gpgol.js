/*
  SPDX-FileCopyrightText: 2009 Constantin Berzan <exit3219@gmail.com>

  SPDX-License-Identifier: LGPL-2.0-or-later
*/

#pragma once

#include <QObject>

#include <KMime/Headers>

namespace MessageComposer
{
/**
 * @brief The MessagePart class
 */
class MessagePart : public QObject
{
    Q_OBJECT

public:
    explicit MessagePart(QObject *parent = nullptr);
    ~MessagePart() override;
};
}
