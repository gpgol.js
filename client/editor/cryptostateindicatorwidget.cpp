/*
   SPDX-FileCopyrightText: 2014-2023 Laurent Montel <montel@kde.org>

   SPDX-License-Identifier: GPL-2.0-or-later
*/

#include "cryptostateindicatorwidget.h"
#include <KColorScheme>

#include <KLocalizedString>

#include <QApplication>
#include <QHBoxLayout>
#include <QLabel>

static bool isLightTheme()
{
    return qApp->palette().color(QPalette::Window).value() >= 128;
}

CryptoStateIndicatorWidget::CryptoStateIndicatorWidget(QWidget *parent)
    : QWidget(parent)
    , mSignatureStateIndicator(new QLabel(this))
    , mEncryptionStateIndicator(new QLabel(this))
{
    KColorScheme scheme(QPalette::Active, KColorScheme::View);

    auto hbox = new QHBoxLayout(this);
    hbox->setContentsMargins({});
    hbox->setSpacing(0);
    mSignatureStateIndicator->setAlignment(Qt::AlignHCenter);
    mSignatureStateIndicator->setTextFormat(Qt::PlainText);
    mSignatureStateIndicator->setContentsMargins(6, 6, 6, 6);
    hbox->addWidget(mSignatureStateIndicator);
    mSignatureStateIndicator->setObjectName(QStringLiteral("signatureindicator"));
    QPalette p(mSignatureStateIndicator->palette());
    p.setColor(QPalette::Window, scheme.background(KColorScheme::PositiveBackground).color());
    p.setColor(QPalette::Text, scheme.foreground(KColorScheme::PositiveText).color());
    mSignatureStateIndicator->setPalette(p);
    mSignatureStateIndicator->setAutoFillBackground(true);

    mEncryptionStateIndicator->setAlignment(Qt::AlignHCenter);
    mEncryptionStateIndicator->setTextFormat(Qt::PlainText);
    mEncryptionStateIndicator->setContentsMargins(6, 6, 6, 6);
    hbox->addWidget(mEncryptionStateIndicator);
    p = mEncryptionStateIndicator->palette();

    if (isLightTheme()) {
        p.setColor(QPalette::Window, QColor(0x00, 0x80, 0xFF).lighter(180));
        p.setColor(QPalette::Text, QColor(0x00, 0x80, 0xFF).darker(200));
    } else {
        p.setColor(QPalette::Window, QColor(0x00, 0x80, 0xFF).darker(300));
        p.setColor(QPalette::Text, QColor(0x00, 0x80, 0xFF).lighter(170));
    }
    mEncryptionStateIndicator->setPalette(p);
    mEncryptionStateIndicator->setAutoFillBackground(true);
    mEncryptionStateIndicator->setObjectName(QStringLiteral("encryptionindicator"));
}

CryptoStateIndicatorWidget::~CryptoStateIndicatorWidget() = default;

void CryptoStateIndicatorWidget::updateSignatureAndEncrypionStateIndicators(bool isSign, bool isEncrypted)
{
    mIsEncrypted = isEncrypted;
    mIsSign = isSign;

    mSignatureStateIndicator->setText(isSign ? i18n("Message will be signed") : i18n("Message will not be signed"));
    mEncryptionStateIndicator->setText(isEncrypted ? i18n("Message will be encrypted") : i18n("Message will not be encrypted"));
}

#include "moc_cryptostateindicatorwidget.cpp"
