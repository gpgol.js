/*
  SPDX-FileCopyrightText: 2009 Constantin Berzan <exit3219@gmail.com>
  SPDX-FileCopyrightText: 2009 Klaralvdalens Datakonsult AB, a KDAB Group company, info@kdab.net
  SPDX-FileCopyrightText: 2009 Leo Franchi <lfranchi@kde.org>

  SPDX-License-Identifier: LGPL-2.0-or-later
*/

#pragma once

#include <KMime/Message>
#include <Libkleo/Enum>

class QTextDocument;

namespace KMime
{
class Content;
}

namespace MessageComposer
{
namespace Util
{
[[nodiscard]] QByteArray selectCharset(const QList<QByteArray> &charsets, const QString &text);

[[nodiscard]] QStringList AttachmentKeywords();
[[nodiscard]] QString cleanedUpHeaderString(const QString &s);

[[nodiscard]] bool sendMailDispatcherIsOnline(QWidget *parent = nullptr);

/**
 * find mimetype in message
 */
[[nodiscard]] KMime::Content *findTypeInMessage(KMime::Content *data, const QByteArray &mimeType, const QByteArray &subType);

[[nodiscard]] bool hasMissingAttachments(const QStringList &attachmentKeywords, QTextDocument *doc, const QString &subj);

[[nodiscard]] QStringList cleanEmailList(const QStringList &emails);
[[nodiscard]] QStringList cleanUpEmailListAndEncoding(const QStringList &emails);
void addCustomHeaders(const KMime::Message::Ptr &message, const QMap<QByteArray, QString> &customHeader);
}
}
