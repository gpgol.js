/*
   SPDX-FileCopyrightText: 2014-2023 Laurent Montel <montel@kde.org>

   SPDX-License-Identifier: LGPL-2.0-or-later
*/

#include "attachmentfromurlutils.h"
#include "attachmentfromfolderjob.h"
#include "attachmentfromurljob.h"
#include "editor_debug.h"
#include "messagecomposersettings.h"

#include <QMimeDatabase>

namespace MessageCore
{
MessageCore::AttachmentFromUrlBaseJob *AttachmentFromUrlUtils::createAttachmentJob(const QUrl &url, QObject *parent)
{
    MessageCore::AttachmentFromUrlBaseJob *ajob = nullptr;
    QMimeDatabase db;
    if (db.mimeTypeForUrl(url).name() == QLatin1String("inode/directory")) {
        qCDebug(EDITOR_LOG) << "Creating attachment from folder";
        ajob = new MessageCore::AttachmentFromFolderJob(url, parent);
    } else {
        ajob = new MessageCore::AttachmentFromUrlJob(url, parent);
        qCDebug(EDITOR_LOG) << "Creating attachment from file";
    }
    if (MessageComposer::MessageComposerSettings::maximumAttachmentSize() > 0) {
        ajob->setMaximumAllowedSize(MessageComposer::MessageComposerSettings::maximumAttachmentSize());
    }
    return ajob;
}
}
