/*
 * This file is part of KMail.
 * SPDX-FileCopyrightText: 2009 Constantin Berzan <exit3219@gmail.com>
 *
 * Parts based on KMail code by:
 * Various authors.
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#pragma once

#include "attachmentcontrollerbase.h"

class QModelIndex;
class ComposerWindow;

namespace MessageComposer
{
class AttachmentModel;
}

class AttachmentView;

class AttachmentController : public MessageComposer::AttachmentControllerBase
{
    Q_OBJECT

public:
    explicit AttachmentController(MessageComposer::AttachmentModel *model, AttachmentView *view, ComposerWindow *composer);
    ~AttachmentController() override;

public Q_SLOTS:
    void attachMyPublicKey() override;

private:
    void identityChanged();
    void slotActionsCreated();
    void selectionChanged();
    void onShowAttachment(KMime::Content *content);
    void doubleClicked(const QModelIndex &itemClicked);
    void slotSelectAllAttachment();

    ComposerWindow *const mComposer;
    AttachmentView *const mView;
};
