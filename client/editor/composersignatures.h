/*
   SPDX-FileCopyrightText: 2015-2023 Laurent Montel <montel@kde.org>

   SPDX-License-Identifier: GPL-2.0-or-later
*/

#pragma once

#include <QObject>
#include <memory>

namespace KIdentityManagementCore
{
class Signature;
}

namespace MessageComposer
{
class BodyTextEditor;

class ComposerSignatures : public QObject
{
    Q_OBJECT
public:
    explicit ComposerSignatures(MessageComposer::BodyTextEditor *textEditor, QObject *parent = nullptr);
    ~ComposerSignatures() override;

    void cleanWhitespace(const KIdentityManagementCore::Signature &sig);

    [[nodiscard]] bool replaceSignature(const KIdentityManagementCore::Signature &oldSig, const KIdentityManagementCore::Signature &newSig);

private:
    void cleanWhitespaceHelper(const QRegularExpression &regExp, const QString &newText, const KIdentityManagementCore::Signature &sig);
    [[nodiscard]] QList<QPair<int, int>> signaturePositions(const KIdentityManagementCore::Signature &sig) const;

    MessageComposer::BodyTextEditor *m_textEditor;
};
}
