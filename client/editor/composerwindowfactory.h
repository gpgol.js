// SPDX-FileCopyrightText: 2023 g10 code Gmbh
// SPDX-Contributor: Carl Schwan <carl.schwan@gnupg.com>
// SPDX-License-Identifier: GPL-2.0-or-later

#pragma once

#include <QHash>
#include <QString>

class QWidget;
class ComposerWindow;

/// Factory to create ComposerWindow.
class ComposerWindowFactory
{
public:
    /// Get factory singleton.
    static ComposerWindowFactory &self();

    /// Create a new composer dialog.
    ///
    /// This might reuse an existing and unused composer dialog instead of creating a
    /// new one.
    ComposerWindow *create(const QString &fromAddress, const QString &name, const QByteArray &bearerToken);

    /// Restore all autosaved files.
    void restoreAutosave(const QString &fromAddress, const QString &name, const QByteArray &bearerToken);

    /// Clear composer dialog metadata when not needed anymore.
    ///
    /// Ensure that there is always at least one QMainWindow active at the same time
    /// so that the QApplication is not deleted.
    void clear(ComposerWindow *composerWindow);

private:
    ComposerWindowFactory();

    ComposerWindow *m_inactiveWindow = nullptr;
    QHash<QString, ComposerWindow *> m_activeWindows;
};
