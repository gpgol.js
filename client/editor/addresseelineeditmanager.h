/*
  SPDX-FileCopyrightText: 2015-2024 Laurent Montel <montel@kde.org>

  SPDX-License-Identifier: GPL-2.0-or-later
*/

#pragma once

#include <QMap>

#include <QList>
#include <QPointer>

#include <QColor>

class QTimer;
class QNetworkConfigurationManager;
class KMailCompletion;
class AddresseeLineEdit;
class AddresseeLineEditGnupg;

class AddresseeLineEditManager
{
public:
    using CompletionItemsMap = QMap<QString, QPair<int, int>>;

    AddresseeLineEditManager();

    ~AddresseeLineEditManager();

    static AddresseeLineEditManager *self();

    int addCompletionSource(const QString &source, int weight);
    void removeCompletionSource(const QString &source);

    CompletionItemsMap completionItemsMap;
    QStringList completionSources;

    // The weights associated with the completion sources in s_static->completionSources.
    // Both are maintained by addCompletionSource(), don't attempt to modify those yourself.
    QMap<QString, int> completionSourceWeights;

    // holds the cached mapping from akonadi collection id to the completion source index
    struct collectionInfo {
        collectionInfo()
            : index(-1)
            , enabled(true)
        {
        }

        collectionInfo(int idx, bool _enabled)
            : index(idx)
            , enabled(_enabled)
        {
        }

        int index;
        bool enabled;
    };

    KMailCompletion *completion() const;

    [[nodiscard]] bool isOnline() const;

    void loadBalooBlackList();
    [[nodiscard]] QStringList cleanupEmailList(const QStringList &inputList);
    [[nodiscard]] QStringList balooBlackList() const;

    bool autoGroupExpand() const;
    void setAutoGroupExpand(bool checked);

    [[nodiscard]] QColor alternateColor() const;

private:
    QStringList mRecentAddressEmailList;
    QStringList mRecentCleanupAddressEmailList;
    mutable QColor mAlternateColor;
    bool mAutoGroupExpand = false;
    std::unique_ptr<KMailCompletion> const mCompletion;
};
