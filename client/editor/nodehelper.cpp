/*
  SPDX-FileCopyrightText: 2010 Klarälvdalens Datakonsult AB, a KDAB Group company, info@kdab.com
  SPDX-FileCopyrightText: 2009 Andras Mantia <andras@kdab.net>
  SPDX-FileCopyrightText: 2010 Leo Franchi <lfranchi@kde.org>

  SPDX-License-Identifier: GPL-2.0-or-later
*/

#include "nodehelper.h"

#include <KMime/Content>
#include <KMime/Message>

KMime::Content *MessageCore::NodeHelper::nextSibling(KMime::Content *node)
{
    if (!node) {
        return nullptr;
    }

    if (KMime::Content *parent = node->parent()) {
        const auto contents = parent->contents();
        const int index = contents.indexOf(node) + 1;
        if (index < contents.size()) { // next on the same level
            return contents.at(index);
        }
    }
    return nullptr;
}

KMime::Content *MessageCore::NodeHelper::next(KMime::Content *node, bool allowChildren)
{
    if (allowChildren) {
        if (KMime::Content *child = firstChild(node)) {
            return child;
        }
    }

    if (KMime::Content *sibling = nextSibling(node)) {
        return sibling;
    }

    for (KMime::Content *parent = node->parent(); parent; parent = parent->parent()) {
        if (KMime::Content *sibling = nextSibling(parent)) {
            return sibling;
        }
    }

    return nullptr;
}

KMime::Content *MessageCore::NodeHelper::firstChild(KMime::Content *node)
{
    if (!node) {
        return nullptr;
    }

    KMime::Content *child = nullptr;
    if (!node->contents().isEmpty()) {
        child = node->contents().at(0);
    }

    return child;
}
