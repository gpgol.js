/*
 * SPDX-FileCopyrightText: 2010 Volker Krause <vkrause@kde.org>
 *
 * Based on kmail/kmcomposewin.cpp
 * SPDX-FileCopyrightText: 2009 Constantin Berzan <exit3219@gmail.com>
 *
 * Based on KMail code by:
 * SPDX-FileCopyrightText: 1997 Markus Wuebben <markus.wuebben@kde.org>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include "signaturecontroller.h"
#include "bodytexteditor.h"
#include "composersignatures.h"
#include "messagecomposersettings.h"

#include "identity/identity.h"

using namespace MessageComposer;

class MessageComposer::SignatureControllerPrivate
{
public:
    SignatureControllerPrivate() = default;

    KIdentityManagementCore::Identity m_currentIdentity;
    KIdentityManagementWidgets::IdentityCombo *m_identityCombo = nullptr;
    BodyTextEditor *m_editor = nullptr;
};

SignatureController::SignatureController(QObject *parent)
    : QObject(parent)
    , d(new MessageComposer::SignatureControllerPrivate)
{
}

SignatureController::~SignatureController() = default;

void SignatureController::setEditor(BodyTextEditor *editor)
{
    d->m_editor = editor;
}

void SignatureController::setIdentity(const KIdentityManagementCore::Identity &identity)
{
    d->m_currentIdentity = identity;
}

void SignatureController::appendSignature()
{
    insertSignatureHelper(KIdentityManagementCore::Signature::End);
}

void SignatureController::prependSignature()
{
    insertSignatureHelper(KIdentityManagementCore::Signature::Start);
}

void SignatureController::insertSignatureAtCursor()
{
    insertSignatureHelper(KIdentityManagementCore::Signature::AtCursor);
}

void SignatureController::cleanSpace()
{
    if (!d->m_editor || d->m_currentIdentity.isNull()) {
        return;
    }
    const KIdentityManagementCore::Signature signature = d->m_currentIdentity.signature();
    d->m_editor->composerSignatures()->cleanWhitespace(signature);
}

void SignatureController::insertSignatureHelper(KIdentityManagementCore::Signature::Placement placement)
{
    if (!d->m_editor || d->m_currentIdentity.isNull()) {
        return;
    }

    // Identity::signature() is not const, although it should be, therefore the
    // const_cast.
    const KIdentityManagementCore::Signature signature = d->m_currentIdentity.signature();

    KIdentityManagementCore::Signature::AddedText addedText = KIdentityManagementCore::Signature::AddNewLines;
    if (MessageComposer::MessageComposerSettings::self()->dashDashSignature()) {
        addedText |= KIdentityManagementCore::Signature::AddSeparator;
    }
    d->m_editor->insertSignature(signature, placement, addedText);
    if ((placement == KIdentityManagementCore::Signature::Start) || (placement == KIdentityManagementCore::Signature::End)) {
        Q_EMIT signatureAdded();
    }
}

void SignatureController::applySignature(const KIdentityManagementCore::Signature &signature)
{
    if (!d->m_editor) {
        return;
    }

    if (MessageComposer::MessageComposerSettings::self()->autoTextSignature() == QLatin1String("auto")) {
        KIdentityManagementCore::Signature::AddedText addedText = KIdentityManagementCore::Signature::AddNewLines;
        if (MessageComposer::MessageComposerSettings::self()->dashDashSignature()) {
            addedText |= KIdentityManagementCore::Signature::AddSeparator;
        }
        if (MessageComposer::MessageComposerSettings::self()->prependSignature()) {
            d->m_editor->insertSignature(signature, KIdentityManagementCore::Signature::Start, addedText);
        } else {
            d->m_editor->insertSignature(signature, KIdentityManagementCore::Signature::End, addedText);
        }
    }
}

#include "moc_signaturecontroller.cpp"
